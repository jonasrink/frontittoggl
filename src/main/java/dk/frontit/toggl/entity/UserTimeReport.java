package dk.frontit.toggl.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class UserTimeReport implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long timeReportId;

    @ManyToOne
    @JoinColumn(name = "userId", foreignKey = @ForeignKey(name = "fk_user_time_report_to_users"))
    private Users users;

    private Long totalTime;
    private Long planedTime;
    private Long workedTime;
    private Long billableTime;
    private Long dayOff;
    private Long vacation;
    private Long illness;

    @OneToMany
    @JoinColumn(name = "timeEntryId", foreignKey = @ForeignKey(name = "fk_user_timer_report_to_time_entries"))
    private List<TimeEntry> timeEntries;

    public Long getTimeReportId() {
        return timeReportId;
    }

    public void setTimeReportId(Long timeReportId) {
        this.timeReportId = timeReportId;
    }

    public Users getUser() {
        return users;
    }

    public void setUser(Users users) {
        this.users = users;
    }

    public Long getPlanedTime() {
        return planedTime;
    }

    public void setPlanedTime(Long planedTime) {
        this.planedTime = planedTime;
    }

    public Long getWorkedTime() {
        return workedTime;
    }

    public void setWorkedTime(Long workedTime) {
        this.workedTime = workedTime;
    }

    public Long getBillableTime() {
        return billableTime;
    }

    public void setBillableTime(Long billableTime) {
        this.billableTime = billableTime;
    }

    public Long getDayOff() {
        return dayOff;
    }

    public void setDayOff(Long dayOff) {
        this.dayOff = dayOff;
    }

    public Long getVacation() {
        return vacation;
    }

    public void setVacation(Long vacation) {
        this.vacation = vacation;
    }

    public Long getIllness() {
        return illness;
    }

    public void setIllness(Long illness) {
        this.illness = illness;
    }

    public List<TimeEntry> getTimeEntries() {
        return timeEntries;
    }

    public void setTimeEntries(List<TimeEntry> timeEntries) {
        this.timeEntries = timeEntries;
    }
}
