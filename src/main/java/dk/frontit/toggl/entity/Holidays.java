package dk.frontit.toggl.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;
import java.util.Date;

@Entity
public class Holidays {
    @Id
    @GeneratedValue
    private Long id;
    private String summary;
    private LocalDate since;
    private LocalDate until;

    public Holidays() {
    }

    public Holidays(String summary, LocalDate since, LocalDate until) {
        this.summary = summary;
        this.since = since;
        this.until = until;
    }

    public String getSummary() {
        return summary;
    }

    public LocalDate getSince() {
        return since;
    }

    public LocalDate getUntil() {
        return until;
    }

    @Override
    public String toString() {
        return "PublicHolidays{" +
                "id=" + id +
                ", summary='" + summary + '\'' +
                ", start=" + since +
                ", end=" + until +
                '}';
    }
}
