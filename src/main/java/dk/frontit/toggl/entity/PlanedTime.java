package dk.frontit.toggl.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class PlanedTime {
    @Id
    @GeneratedValue
    private Long id;

    private Long workHoursPerweek;
    private LocalDate since;
    private LocalDate until;
    @ManyToOne
    @JoinColumn(name = "userId", foreignKey = @ForeignKey(name = "fk_planed_time_to_users"))
    private Users users;

    public PlanedTime(Long workHoursPerweek, LocalDate since, LocalDate until, Users users) {
        this.workHoursPerweek = workHoursPerweek;
        this.since = since;
        this.until = until;
        this.users = users;
    }

    public PlanedTime(Long workHoursPerweek, LocalDate since, Users users) {
        this.workHoursPerweek = workHoursPerweek;
        this.since = since;
        this.users = users;
    }

    public PlanedTime() {

    }


    public Long getId() {
        return id;
    }

    public Long getWorkHoursPerweek() {
        return workHoursPerweek;
    }

    public LocalDate getSince() {
        return since;
    }

    public LocalDate getUntil() {
        return until;
    }

    public void setUntil(LocalDate until) {
        this.until = until;
    }

    @Override
    public String toString() {
        return "PlanedTime{" +
                "id=" + id +
                ", workHoursPerweek=" + workHoursPerweek +
                ", since=" + since +
                ", until=" + until +
                '}';
    }

    public void setWorkHoursPerweek(Long workHoursPerweek) {
        this.workHoursPerweek = workHoursPerweek;
    }

    public void setSince(LocalDate since) {
        this.since = since;
    }

    public Users getUser() {
        return users;
    }

    public void setUser(Users users) {
        this.users = users;
    }
}
