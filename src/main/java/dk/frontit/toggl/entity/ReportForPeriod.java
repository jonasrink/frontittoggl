package dk.frontit.toggl.entity;

import dk.frontit.toggl.model.Status;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
public class ReportForPeriod implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Date periodStart;
    private Date periodEnd;
    private Status status;

    @ManyToMany
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @JoinTable(name = "ReportToTimeEntry",
            joinColumns = {@JoinColumn(name = "reportForPeriodId")},foreignKey = @ForeignKey(name = "fk_report_to_time_entry_to_report_for_period"),
            inverseJoinColumns = {@JoinColumn(name = "timeEntryId")}, inverseForeignKey = @ForeignKey(name = "fk_report_to_time_entry_to_time_entry"))
    private List<TimeEntry> timeEntries;

    public ReportForPeriod(Date periodStart, Date periodEnd, Status status, List<TimeEntry> timeEntries) {
        this.periodStart = periodStart;
        this.periodEnd = periodEnd;
        this.status = status;
        this.timeEntries = timeEntries;
    }

    public ReportForPeriod() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getPeriodStart() {
        return periodStart;
    }

    public void setPeriodStart(Date periodStart) {
        this.periodStart = periodStart;
    }

    public Date getPeriodEnd() {
        return periodEnd;
    }

    public void setPeriodEnd(Date periodEnd) {
        this.periodEnd = periodEnd;
    }

    public List<TimeEntry> getTimeEntries() {
        return timeEntries;
    }

    public void addTimeEntry(TimeEntry timeEntry) {
        if (timeEntries == null) {
            timeEntries = new ArrayList<>();
        }
        timeEntries.add(timeEntry);
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
