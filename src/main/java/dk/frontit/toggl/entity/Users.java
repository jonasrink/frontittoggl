package dk.frontit.toggl.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Users implements Serializable {
    private static final long MILLIS_IN_A_DAY = 1000 * 60 * 60 * 24;
    @Id
    private Long id;
    private Long defaultWid;
    private String email;
    private String fullname;
    private String jqueryTimeofdayFormat;
    private String jqueryDateFormat;
    private String timeofdayFormat;
    private String dateFormat;
    private boolean storeStartAndStopTime;
    private int beginningOfWeek;
    private String language;
    private Date at;
    private Date createdAt;
    private int retention;
    private String timezone;
    private String isAdmin;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "userId", foreignKey = @ForeignKey(name = "fk_users_to_planed_time"))
    private List<PlanedTime> planedTime;

    public Users() {
    }

    public Users(Long id, Long defaultWid, String email, String fullName, String jqueryTimeOfDayFormat, String jqueryDateFormat, String timeofdayFormat, String dateFormat, boolean storeStartAndStopTime, int beginningOfWeek, String language, Date at, Date createdAt, int retention, String timezone) {
        this.id = id;
        this.defaultWid = defaultWid;
        this.email = email;
        this.fullname = fullName;
        this.jqueryTimeofdayFormat = jqueryTimeOfDayFormat;
        this.jqueryDateFormat = jqueryDateFormat;
        this.timeofdayFormat = timeofdayFormat;
        this.dateFormat = dateFormat;
        this.storeStartAndStopTime = storeStartAndStopTime;
        this.beginningOfWeek = beginningOfWeek;
        this.language = language;
        this.at = at;
        this.createdAt = createdAt;
        this.retention = retention;
        this.timezone = timezone;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDefaultWid() {
        return defaultWid;
    }

    public void setDefaultWid(Long defaultWid) {
        this.defaultWid = defaultWid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getJqueryTimeofdayFormat() {
        return jqueryTimeofdayFormat;
    }

    public void setJqueryTimeofdayFormat(String jqueryTimeofdayFormat) {
        this.jqueryTimeofdayFormat = jqueryTimeofdayFormat;
    }

    public String getJqueryDateFormat() {
        return jqueryDateFormat;
    }

    public void setJqueryDateFormat(String jqueryDateFormat) {
        this.jqueryDateFormat = jqueryDateFormat;
    }

    public String getTimeofdayFormat() {
        return timeofdayFormat;
    }

    public void setTimeofdayFormat(String timeofdayFormat) {
        this.timeofdayFormat = timeofdayFormat;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public boolean isStoreStartAndStopTime() {
        return storeStartAndStopTime;
    }

    public void setStoreStartAndStopTime(boolean storeStartAndStopTime) {
        this.storeStartAndStopTime = storeStartAndStopTime;
    }

    public int getBeginningOfWeek() {
        return beginningOfWeek;
    }

    public void setBeginningOfWeek(int beginningOfWeek) {
        this.beginningOfWeek = beginningOfWeek;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Date getAt() {
        return at;
    }

    public void setAt(Date at) {
        this.at = at;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getRetention() {
        return retention;
    }

    public void setRetention(int retention) {
        this.retention = retention;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }

    public List<PlanedTime> getPlanedTime() {
        return planedTime;
    }

    public void addPlanedTime(PlanedTime planedTime) {
        if (this.planedTime == null) {
            this.planedTime = new ArrayList<>();
        }
        this.planedTime.stream()
                .filter(planedTime1 -> planedTime1.getUntil() == null)
                .findFirst().ifPresent(planedTime1 -> planedTime1.setUntil(planedTime.getSince().minusDays(1)));
        this.planedTime.add(planedTime);
    }
}
