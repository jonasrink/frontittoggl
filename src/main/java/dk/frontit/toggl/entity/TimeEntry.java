package dk.frontit.toggl.entity;

import dk.frontit.toggl.model.Status;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Entity
public class TimeEntry implements Serializable {
    private static final long serialVersionUID = -1798070786993154676L;

    @Id
    @Column(name = "id")
    private Long id;
    private String description;

    @ManyToOne
    @JoinColumn(name = "projectId", foreignKey = @ForeignKey(name = "fk_time_entry_to_project"))
    private Project project;
    private Date start;
    private Date stop;
    private Long duration;
    private Boolean isBillable;
    private String client;
    private String workspace;
    private String createdWith;
    private Boolean durOnly;
    private Long pid;
    private Long wid;
    private Long tid;

    @Enumerated(EnumType.STRING)
    private Status status;
    private String tagNames;

    @ManyToOne
    @JoinColumn(name = "userId", foreignKey = @ForeignKey(name = "fk_time_entry_to_users"))
    private Users users;

    public TimeEntry() {
    }

    public TimeEntry(Long id, String description, Project project, Date start, Date stop, Long duration, Boolean isBillable, String client, String workspace, String createdWith, Boolean durOnly, Long pid, Long wid, Long tid, Status status, Users users, List<String> tagNames) {
        this.id = id;
        this.description = description;
        this.project = project;
        this.start = start;
        this.stop = stop;
        this.duration = duration;
        this.isBillable = isBillable;
        this.client = client;
        this.workspace = workspace;
        this.createdWith = createdWith;
        this.durOnly = durOnly;
        this.pid = pid;
        this.wid = wid;
        this.tid = tid;
        this.status = status;
        this.users = users;
        if (tagNames != null) {
            this.tagNames = tagNames.stream().collect(Collectors.joining(","));
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getStop() {
        return stop;
    }

    public void setStop(Date stop) {
        this.stop = stop;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Boolean getBillable() {
        return isBillable;
    }

    public void setBillable(Boolean billable) {
        isBillable = billable;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getWorkspace() {
        return workspace;
    }

    public void setWorkspace(String workspace) {
        this.workspace = workspace;
    }

    public String getCreatedWith() {
        return createdWith;
    }

    public void setCreatedWith(String createdWith) {
        this.createdWith = createdWith;
    }

    public Boolean getDurOnly() {
        return durOnly;
    }

    public void setDurOnly(Boolean duronly) {
        this.durOnly = duronly;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getWid() {
        return wid;
    }

    public void setWid(Long wid) {
        this.wid = wid;
    }

    public Long getTid() {
        return tid;
    }

    public void setTid(Long tid) {
        this.tid = tid;
    }

    public Users getUser() {
        return users;
    }

    public void setUser(Users users) {
        this.users = users;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getTagNames() {
        return tagNames;
    }

    public void setTagNames(String tagNames) {
        this.tagNames = tagNames;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeEntry timeEntry = (TimeEntry) o;
        return id.equals(timeEntry.id) && Objects.equals(description, timeEntry.description) && Objects.equals(project, timeEntry.project) && start.equals(timeEntry.start) && Objects.equals(stop, timeEntry.stop) && Objects.equals(duration, timeEntry.duration) && Objects.equals(isBillable, timeEntry.isBillable) && Objects.equals(client, timeEntry.client) && Objects.equals(workspace, timeEntry.workspace) && Objects.equals(createdWith, timeEntry.createdWith) && Objects.equals(durOnly, timeEntry.durOnly) && Objects.equals(pid, timeEntry.pid) && Objects.equals(wid, timeEntry.wid) && Objects.equals(tid, timeEntry.tid) && Objects.equals(tagNames, timeEntry.tagNames) && users.equals(timeEntry.users);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, project, start, stop, duration, isBillable, client, workspace, createdWith, durOnly, pid, wid, tid, tagNames, users);
    }
}
