package dk.frontit.toggl.entity;

import dk.frontit.toggl.model.TaskType;

import javax.persistence.*;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(name = "uk_taskid", columnNames = {"taskId"}))
public class SpecialTasks {
    @Id
    @GeneratedValue
    private Long id;


    private Long taskId;

    @Enumerated(EnumType.STRING)
    private TaskType taskType;


    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
