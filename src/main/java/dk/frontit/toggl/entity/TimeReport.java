package dk.frontit.toggl.entity;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class TimeReport extends ReportForPeriod {

    @OneToMany
    @JoinColumn(name = "userTimeReportsId", foreignKey = @ForeignKey(name = "fk_time_report_to_user_time_report"))
    List<UserTimeReport> usersTimeReports;
}
