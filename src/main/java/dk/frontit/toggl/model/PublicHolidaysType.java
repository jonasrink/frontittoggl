package dk.frontit.toggl.model;

public enum PublicHolidaysType {
    NewYearsEve("Naujieji metai"),
    StateRestorationDay("Lietuvos valstybės atkūrimo diena"),
    IndependenceRestorationDay("Lietuvos nepriklausomybės atkūrimo diena"),
    Easter("Velykos"),
    LabourDay("Tarptautinė darbo diena"),
    JohnsDay("Joninės/Rasos"),
    StateHoodDay("Valstybės (Lietuvos karaliaus Mindaugo karūnavimo) diena"),
    AssumptionDay("Žolinė (Švč. Mergelės Marijos ėmimo į dangų diena)"),
    SaintsDay("Visų Šventųjų diena"),
    SoulsDay("Mirusiųjų atminimo (Vėlinių) diena"),
    ChristmasEve("Kūčios"),
    Christmas("Kalėdos");

    private String summary;

    PublicHolidaysType(String summary) {

        this.summary = summary;
    }

    public String getSummary() {
        return summary;
    }
}
