package dk.frontit.toggl.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.Objects;

public class ProjectView {
    public int id;
    public String guid;
    public int wid;
    public String name;
    public boolean billable;
    @JsonProperty("is_private")
    public boolean isPrivate;
    public boolean active;
    public boolean template;
    public Date at;
    @JsonProperty("created_at")
    public Date createdAt;
    public String color;
    @JsonProperty("auto_estimates")
    public boolean autoEstimates;
    @JsonProperty("actual_hours")
    public int actualHours;
    @JsonProperty("hex_color")
    public String hexColor;
    public int cid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public int getWid() {
        return wid;
    }

    public void setWid(int wid) {
        this.wid = wid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isBillable() {
        return billable;
    }

    public void setBillable(boolean billable) {
        this.billable = billable;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isTemplate() {
        return template;
    }

    public void setTemplate(boolean template) {
        this.template = template;
    }

    public Date getAt() {
        return at;
    }

    public void setAt(Date at) {
        this.at = at;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isAutoEstimates() {
        return autoEstimates;
    }

    public void setAutoEstimates(boolean autoEstimates) {
        this.autoEstimates = autoEstimates;
    }

    public int getActualHours() {
        return actualHours;
    }

    public void setActualHours(int actualHours) {
        this.actualHours = actualHours;
    }

    public String getHexColor() {
        return hexColor;
    }

    public void setHexColor(String hexColor) {
        this.hexColor = hexColor;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProjectView that = (ProjectView) o;
        return id == that.id && wid == that.wid && billable == that.billable && isPrivate == that.isPrivate && active == that.active && template == that.template && autoEstimates == that.autoEstimates && actualHours == that.actualHours && cid == that.cid && Objects.equals(guid, that.guid) && Objects.equals(name, that.name) && Objects.equals(at, that.at) && Objects.equals(createdAt, that.createdAt) && Objects.equals(color, that.color) && Objects.equals(hexColor, that.hexColor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, guid, wid, name, billable, isPrivate, active, template, at, createdAt, color, autoEstimates, actualHours, hexColor, cid);
    }

    @Override
    public String toString() {
        return "ProjectView{" +
                "id=" + id +
                ", guid='" + guid + '\'' +
                ", wid=" + wid +
                ", name='" + name + '\'' +
                ", billable=" + billable +
                ", isPrivate=" + isPrivate +
                ", active=" + active +
                ", template=" + template +
                ", at=" + at +
                ", createdAt=" + createdAt +
                ", color='" + color + '\'' +
                ", autoEstimates=" + autoEstimates +
                ", actualHours=" + actualHours +
                ", hexColor='" + hexColor + '\'' +
                ", cid=" + cid +
                '}';
    }
}
