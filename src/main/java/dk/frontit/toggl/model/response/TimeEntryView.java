package dk.frontit.toggl.model.response;


import com.fasterxml.jackson.annotation.JsonProperty;
import dk.frontit.toggl.model.Status;

import java.text.SimpleDateFormat;
import java.util.*;

public class TimeEntryView {

    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private Long id;
    private String description;
    private String project;
    private Date start;
    @JsonProperty("end")
    private Date stop;
    @JsonProperty("dur")
    private Long duration;
    private double billable;
    @JsonProperty("is_billable")
    private Boolean isBillable;
    private String client;
    private String workspace;
    private List<String> tagNames = new ArrayList();
    private String created_with;
    private Boolean duronly;
    private Long pid;
    private Long wid;
    private Long tid;
    private Long uid;
    private Status status;


    public TimeEntryView() {
    }

    public TimeEntryView(Long id, String description, String project, Date start, Date stop, Long duration, Boolean isBillable, String client, String workspace, List<String> tagNames, String created_with, Boolean duronly, Long pid, Long wid, Long tid, Long uid) {
        this.id = id;
        this.description = description;
        this.project = project;
        this.start = start;
        this.stop = stop;
        this.duration = duration;
        this.isBillable = isBillable;
        this.client = client;
        this.workspace = workspace;
        this.tagNames = tagNames;
        this.created_with = created_with;
        this.duronly = duronly;
        this.pid = pid;
        this.wid = wid;
        this.tid = tid;
        this.uid = uid;
    }

    public TimeEntryView(Long id, String description, String project, Date start, Date stop, Long duration, Boolean isBillable, String client, String workspace, String tagNames, String created_with, Boolean duronly, Long pid, Long wid, Long tid, Long uid, Status status) {
        this.id = id;
        this.description = description;
        this.project = project;
        this.start = start;
        this.stop = stop;
        this.duration = duration;
        this.isBillable = isBillable;
        this.client = client;
        this.workspace = workspace;
        if (!tagNames.isEmpty()) {
            this.tagNames = Arrays.asList(tagNames.split(";"));
        }
        this.created_with = created_with;
        this.duronly = duronly;
        this.pid = pid;
        this.wid = wid;
        this.tid = tid;
        this.uid = uid;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getStop() {
        return stop;
    }

    public void setStop(Date stop) {
        this.stop = stop;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Boolean getBillable() {
        return isBillable;
    }

    public void setBillable(Boolean billable) {
        isBillable = billable;
    }

    public List<String> getTagNames() {
        return tagNames;
    }

    public void setTagNames(List<String> tagNames) {
        this.tagNames = tagNames;
    }

    public String getCreated_with() {
        return created_with;
    }

    public void setCreated_with(String created_with) {
        this.created_with = created_with;
    }

    public Boolean getDuronly() {
        return duronly;
    }

    public void setDuronly(Boolean duronly) {
        this.duronly = duronly;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getWid() {
        return wid;
    }

    public void setWid(Long wid) {
        this.wid = wid;
    }

    public Long getTid() {
        return tid;
    }

    public void setTid(Long tid) {
        this.tid = tid;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getWorkspace() {
        return workspace;
    }

    public void setWorkspace(String workspace) {
        this.workspace = workspace;
    }

    public static String getDateFormat() {
        return DATE_FORMAT;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "TimeEntryView{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", project=" + project +
                ", start=" + getDate(start) +
                ", stop=" + stop +
                ", duration=" + duration +
                ", isBillable=" + isBillable +
                ", workspace=" + workspace +
                ", tagNames=" + tagNames +
                ", created_with='" + created_with + '\'' +
                ", duronly=" + duronly +
                ", pid=" + pid +
                ", wid=" + wid +
                ", tid=" + tid +
                ", uid=" + uid +
                '}';
    }

    private String getDate(Date date) {
        if(date != null) {
            return new SimpleDateFormat(DATE_FORMAT).format(date);
        }else {
            return "";
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeEntryView that = (TimeEntryView) o;
        return Objects.equals(id, that.id) && Objects.equals(description, that.description) && Objects.equals(project, that.project) && Objects.equals(start, that.start) && Objects.equals(stop, that.stop) && Objects.equals(duration, that.duration) && Objects.equals(isBillable, that.isBillable) && Objects.equals(workspace, that.workspace) && Objects.equals(tagNames, that.tagNames) && Objects.equals(created_with, that.created_with) && Objects.equals(duronly, that.duronly) && Objects.equals(pid, that.pid) && Objects.equals(wid, that.wid) && Objects.equals(tid, that.tid) && Objects.equals(uid, that.uid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, project, start, stop, duration, isBillable, workspace, tagNames, created_with, duronly, pid, wid, tid, uid);
    }

    public void setBillable(double billable) {
        this.billable = billable;
    }
}
