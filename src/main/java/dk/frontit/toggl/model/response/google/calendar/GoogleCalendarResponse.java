package dk.frontit.toggl.model.response.google.calendar;

import java.util.Date;
import java.util.List;

public class GoogleCalendarResponse {
    public String kind;
    public String etag;
    public String summary;
    public List<Item> items;


    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
