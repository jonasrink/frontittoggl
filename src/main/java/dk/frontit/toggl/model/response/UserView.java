package dk.frontit.toggl.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import dk.frontit.toggl.entity.PlanedTime;
import dk.frontit.toggl.model.PlanedTimeView;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public class UserView {
    private Long id;
    @JsonProperty("default_wid")
    private Long defaultWid;
    private String email;
    private String fullname;

    @JsonProperty("jquery_timeofday_format")
    private String jqueryTimeofdayFormat;
    @JsonProperty("jquery_date_format")
    private String jqueryDateFormat;
    @JsonProperty("timeofday_format")
    private String timeofdayFormat;
    @JsonProperty("date_format")
    private String dateFormat;
    @JsonProperty("store_start_and_stop_time")
    private boolean storeStartAndStopTime;
    @JsonProperty("beginning_of_week")
    private int beginningOfWeek;
    private String language;
    private Date at;
    @JsonProperty("created_at")
    private Date createdAt;
    private int retention;
    private String timezone;
    private List<PlanedTimeView> planedTimeList;
    public UserView() {
    }

    public UserView(Long id, Long defaultWid, String email, String fullname, String jqueryTimeofdayFormat, String jqueryDateFormat, String timeofdayFormat, String dateFormat, boolean storeStartAndStopTime, int beginningOfWeek, String language, Date at, Date createdAt, int retention, String timezone, List<PlanedTimeView> planedTimeView) {
        this.id = id;
        this.defaultWid = defaultWid;
        this.email = email;
        this.fullname = fullname;
        this.jqueryTimeofdayFormat = jqueryTimeofdayFormat;
        this.jqueryDateFormat = jqueryDateFormat;
        this.timeofdayFormat = timeofdayFormat;
        this.dateFormat = dateFormat;
        this.storeStartAndStopTime = storeStartAndStopTime;
        this.beginningOfWeek = beginningOfWeek;
        this.language = language;
        this.at = at;
        this.createdAt = createdAt;
        this.retention = retention;
        this.timezone = timezone;
        this.planedTimeList = planedTimeView;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDefaultWid() {
        return defaultWid;
    }

    public void setDefaultWid(Long defaultWid) {
        this.defaultWid = defaultWid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getJqueryTimeofdayFormat() {
        return jqueryTimeofdayFormat;
    }

    public void setJqueryTimeofdayFormat(String jqueryTimeofdayFormat) {
        this.jqueryTimeofdayFormat = jqueryTimeofdayFormat;
    }

    public String getJqueryDateFormat() {
        return jqueryDateFormat;
    }

    public void setJqueryDateFormat(String jqueryDateFormat) {
        this.jqueryDateFormat = jqueryDateFormat;
    }

    public String getTimeofdayFormat() {
        return timeofdayFormat;
    }

    public void setTimeofdayFormat(String timeofdayFormat) {
        this.timeofdayFormat = timeofdayFormat;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public boolean isStoreStartAndStopTime() {
        return storeStartAndStopTime;
    }

    public void setStoreStartAndStopTime(boolean storeStartAndStopTime) {
        this.storeStartAndStopTime = storeStartAndStopTime;
    }

    public int getBeginningOfWeek() {
        return beginningOfWeek;
    }

    public void setBeginningOfWeek(int beginningOfWeek) {
        this.beginningOfWeek = beginningOfWeek;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Date getAt() {
        return at;
    }

    public void setAt(Date at) {
        this.at = at;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public int getRetention() {
        return retention;
    }

    public void setRetention(int retention) {
        this.retention = retention;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public List<PlanedTimeView> getPlanedTimeList() {
        return planedTimeList;
    }

    public void addPlanedTimeList(List<PlanedTimeView> planedTimeList) {
        this.planedTimeList = planedTimeList;
    }

    @Override
    public String toString() {
        return "UserView{" +
                "id=" + id +
                ", defaultWid=" + defaultWid +
                ", email='" + email + '\'' +
                ", fullname='" + fullname + '\'' +
                ", jqueryTimeofdayFormat='" + jqueryTimeofdayFormat + '\'' +
                ", jqueryDateFormat='" + jqueryDateFormat + '\'' +
                ", timeofdayFormat='" + timeofdayFormat + '\'' +
                ", dateFormat='" + dateFormat + '\'' +
                ", storeStartAndStopTime=" + storeStartAndStopTime +
                ", beginningOfWeek=" + beginningOfWeek +
                ", language='" + language + '\'' +
                ", at=" + at +
                ", createdAt=" + createdAt +
                ", retention=" + retention +
                ", timezone='" + timezone + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserView userView = (UserView) o;
        return id == userView.id && defaultWid == userView.defaultWid && storeStartAndStopTime == userView.storeStartAndStopTime && beginningOfWeek == userView.beginningOfWeek && retention == userView.retention && Objects.equals(email, userView.email) && Objects.equals(fullname, userView.fullname) && Objects.equals(jqueryTimeofdayFormat, userView.jqueryTimeofdayFormat) && Objects.equals(jqueryDateFormat, userView.jqueryDateFormat) && Objects.equals(timeofdayFormat, userView.timeofdayFormat) && Objects.equals(dateFormat, userView.dateFormat) && Objects.equals(language, userView.language) && Objects.equals(at, userView.at) && Objects.equals(createdAt, userView.createdAt) && Objects.equals(timezone, userView.timezone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, defaultWid, email, fullname, jqueryTimeofdayFormat, jqueryDateFormat, timeofdayFormat, dateFormat, storeStartAndStopTime, beginningOfWeek, language, at, createdAt, retention, timezone);
    }
}
