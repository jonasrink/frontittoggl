package dk.frontit.toggl.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ReportResponse {
    @JsonProperty("total_grand")
    private int totalGrand;
    @JsonProperty("total_billable")
    private int totalBillable;
    @JsonProperty("total_currencies")
    private List<TotalCurrencyView> totalCurrencies;
    @JsonProperty("total_count")
    private int totalCount;
    @JsonProperty("per_page")
    private int perPage;
    @JsonProperty("data")
    private List<TimeEntryView> timeEntryViews;

    public int getTotalGrand() {
        return totalGrand;
    }

    public void setTotalGrand(int totalGrand) {
        this.totalGrand = totalGrand;
    }

    public int getTotalBillable() {
        return totalBillable;
    }

    public void setTotalBillable(int totalBillable) {
        this.totalBillable = totalBillable;
    }

    public List<TotalCurrencyView> getTotalCurrencies() {
        return totalCurrencies;
    }

    public void setTotalCurrencies(List<TotalCurrencyView> totalCurrencies) {
        this.totalCurrencies = totalCurrencies;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public List<TimeEntryView> getTimeEntryViews() {
        return timeEntryViews;
    }

    public void setTimeEntryViews(List<TimeEntryView> timeEntryViews) {
        this.timeEntryViews = timeEntryViews;
    }
}

