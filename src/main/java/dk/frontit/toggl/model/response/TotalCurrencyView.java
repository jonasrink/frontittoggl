package dk.frontit.toggl.model.response;

public class TotalCurrencyView {

    private Object currency;
    private Object amount;

    public Object getCurrency() {
        return currency;
    }

    public void setCurrency(Object currency) {
        this.currency = currency;
    }

    public Object getAmount() {
        return amount;
    }

    public void setAmount(Object amount) {
        this.amount = amount;
    }
}
