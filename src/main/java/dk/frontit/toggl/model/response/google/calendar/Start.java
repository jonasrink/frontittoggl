package dk.frontit.toggl.model.response.google.calendar;

public class Start {
    public String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
