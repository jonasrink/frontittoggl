package dk.frontit.toggl.model;

import dk.frontit.toggl.model.response.TimeEntryView;
import dk.frontit.toggl.model.response.UserView;

import java.util.List;
import java.util.Objects;

public class UserTimeReportView {

    private Long timeReportId;
    private UserView user;
    private Long planedTime;
    private Long workedTime;
    private Long billableTime;
    private Long dayOff;
    private Long vacation;
    private Long illness;
    private List<TimeEntryView> timeEntries;

    public UserTimeReportView() {
    }

    public UserTimeReportView(Long timeReportId, UserView user, Long planedTime, Long workedTime, Long billableTime, Long dayOff, Long vacation, Long illness, List<TimeEntryView> timeEntries) {
        this.timeReportId = timeReportId;
        this.user = user;
        this.planedTime = planedTime;
        this.workedTime = workedTime;
        this.billableTime = billableTime;
        this.dayOff = dayOff;
        this.vacation = vacation;
        this.illness = illness;
        this.timeEntries = timeEntries;
    }

    public Long getTimeReportId() {
        return timeReportId;
    }

    public void setTimeReportId(Long timeReportId) {
        this.timeReportId = timeReportId;
    }

    public UserView getUser() {
        return user;
    }

    public void setUser(UserView user) {
        this.user = user;
    }

    public Long getPlanedTime() {
        return planedTime;
    }

    public void setPlanedTime(Long planedTime) {
        this.planedTime = planedTime;
    }

    public Long getWorkedTime() {
        return workedTime;
    }

    public void setWorkedTime(Long workedTime) {
        this.workedTime = workedTime;
    }

    public Long getBillableTime() {
        return billableTime;
    }

    public void setBillableTime(Long billableTime) {
        this.billableTime = billableTime;
    }

    public Long getDayOff() {
        return dayOff;
    }

    public void setDayOff(Long dayOff) {
        this.dayOff = dayOff;
    }

    public Long getVacation() {
        return vacation;
    }

    public void setVacation(Long vacation) {
        this.vacation = vacation;
    }

    public Long getIllness() {
        return illness;
    }

    public void setIllness(Long illness) {
        this.illness = illness;
    }

    public List<TimeEntryView> getTimeEntries() {
        return timeEntries;
    }

    public void setTimeEntries(List<TimeEntryView> timeEntries) {
        this.timeEntries = timeEntries;
    }

    @Override
    public String toString() {
        return "UserTimeReportView{" +
                "timeReportId=" + timeReportId +
//                ", user=" + user +
                ", planedTime=" + planedTime +
                ", workedTime=" + workedTime +
                ", billableTime=" + billableTime +
                ", dayOff=" + dayOff +
                ", vacation=" + vacation +
                ", illness=" + illness +
//                ", timeEntries=" + timeEntries +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserTimeReportView that = (UserTimeReportView) o;
        return Objects.equals(timeReportId, that.timeReportId) && Objects.equals(user, that.user) && Objects.equals(planedTime, that.planedTime) && Objects.equals(workedTime, that.workedTime) && Objects.equals(billableTime, that.billableTime) && Objects.equals(dayOff, that.dayOff) && Objects.equals(vacation, that.vacation) && Objects.equals(illness, that.illness) && Objects.equals(timeEntries, that.timeEntries);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timeReportId, user, planedTime, workedTime, billableTime, dayOff, vacation, illness, timeEntries);
    }
}
