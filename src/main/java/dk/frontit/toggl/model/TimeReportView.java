package dk.frontit.toggl.model;

import dk.frontit.toggl.model.response.TimeEntryView;

import java.util.Date;
import java.util.List;

public class TimeReportView extends ReportForPeriodView{

    private List<UserTimeReportView> usersTimeReportViews;
    public TimeReportView(Long id, Date periodStart, Date periodEnd, Status status, List<TimeEntryView> timeEntries) {
        super(id, periodStart, periodEnd, status, timeEntries);
    }

    public TimeReportView(Date periodStart, Date periodEnd, Status status, List<TimeEntryView> timeEntries) {
        super(periodStart, periodEnd, status, timeEntries);
    }

    public TimeReportView(Date periodStart, Date periodEnd, Status status, List<TimeEntryView> timeEntries, List<UserTimeReportView> usersTimeReportViews) {
        super(periodStart, periodEnd, status, timeEntries);
        this.usersTimeReportViews = usersTimeReportViews;
    }

    public List<UserTimeReportView> getUsersTimeReportViews() {
        return usersTimeReportViews;
    }

    public void setUsersTimeReportViews(List<UserTimeReportView> usersTimeReportViews) {
        this.usersTimeReportViews = usersTimeReportViews;
    }

}
