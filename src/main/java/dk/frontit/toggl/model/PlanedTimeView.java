package dk.frontit.toggl.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import dk.frontit.toggl.model.response.UserView;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;


public class PlanedTimeView {

    private Long id;

    private Long workHoursPerweek;
    private LocalDate since;
    private LocalDate until;
    @JsonProperty("user")
    private UserView userView;

    public PlanedTimeView(Long id, Long workHoursPerweek, LocalDate since, LocalDate until, UserView userView) {
        this.id = id;
        this.workHoursPerweek = workHoursPerweek;
        this.since = since;
        this.until = until;
        this.userView = userView;
    }

    public PlanedTimeView(Long workHoursPerweek, LocalDate since, LocalDate until, UserView userView) {
        this.workHoursPerweek = workHoursPerweek;
        this.since = since;
        this.until = until;
        this.userView = userView;
    }

    public PlanedTimeView(Long workHoursPerweek, LocalDate since, UserView userView) {
        this.workHoursPerweek = workHoursPerweek;
        this.since = since;
        this.userView = userView;
    }

    public PlanedTimeView() {

    }


    public Long getId() {
        return id;
    }

    public Long getWorkHoursPerweek() {
        return workHoursPerweek;
    }

    public LocalDate getSince() {
        return since;
    }

    public LocalDate getUntil() {
        return until;
    }

    public void setUntil(LocalDate until) {
        this.until = until;
    }

    public UserView getUserView() {
        return userView;
    }

    public void setUserView(UserView userView) {
        this.userView = userView;
    }

    @Override
    public String toString() {
        return "PlanedTime{" +
                "id=" + id +
                ", workHoursPerweek=" + workHoursPerweek +
                ", since=" + since +
                ", until=" + until +
                '}';
    }
}
