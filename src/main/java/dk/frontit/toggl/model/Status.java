package dk.frontit.toggl.model;

public enum Status {
    FIXED, DELETED, UNFIXED, NONE;

}
