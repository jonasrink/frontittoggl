package dk.frontit.toggl.model;


import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


public class ReportRequest {


    private String since;
    private String until;
    private Set<Long> projectIds;
    private Set<Long> userIds;

    public String getSince() {
        return since;
    }

    public ReportRequest setSince(String since) {
        this.since = since;
        return this;
    }

    public String getUntil() {
        return until;
    }

    public ReportRequest setUntil(String until) {
        this.until = until;
        return this;
    }

    public ReportRequest addProjectId(Long projectId){
        if(projectIds == null){
            projectIds = new HashSet<>();
        }
        projectIds.add(projectId);
        return this;
    }

    public Set<Long> getUserIds() {
        return userIds;
    }

    public ReportRequest addUserId(Set<Long> userId) {
        if (userIds == null) {
            userIds = new HashSet<>();
        }
        userIds.addAll(userId);
        return this;
    }

    public String toParamList() {
        StringBuilder result = new StringBuilder();
        if (this.since != null) {
            result.append("&since=").append(this.since);
        }

        if (this.until != null) {
            result.append("&until=").append(this.until);
        }

        if (this.userIds != null) {
            result.append("&user_ids=")
                    .append((String) this.userIds.stream()
                            .map(String::valueOf).
                                    collect(Collectors.joining(",")));
        }
        if (this.projectIds != null) {
            result.append("&project_ids=")
                    .append((String) this.projectIds.stream()
                            .map(String::valueOf).
                                    collect(Collectors.joining(",")));
        }
        return result.toString();
    }

}
