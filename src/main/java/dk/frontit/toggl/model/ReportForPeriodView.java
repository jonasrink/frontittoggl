package dk.frontit.toggl.model;

import dk.frontit.toggl.entity.TimeEntry;
import dk.frontit.toggl.model.response.TimeEntryView;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class ReportForPeriodView {

    private Long id;
    private Date periodStart;
    private Date periodEnd;
    private Status status;
    private List<TimeEntryView> timeEntries;

    public ReportForPeriodView(Long id, Date periodStart, Date periodEnd, Status status, List<TimeEntryView> timeEntries) {
        this.id = id;
        this.periodStart = periodStart;
        this.periodEnd = periodEnd;
        this.status = status;
        this.timeEntries = timeEntries;
    }

    public ReportForPeriodView(Date periodStart, Date periodEnd, Status status, List<TimeEntryView> timeEntries) {
        this.periodStart = periodStart;
        this.periodEnd = periodEnd;
        this.status = status;
        this.timeEntries = timeEntries;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getPeriodStart() {
        return periodStart;
    }

    public void setPeriodStart(Date periodStart) {
        this.periodStart = periodStart;
    }

    public Date getPeriodEnd() {
        return periodEnd;
    }

    public void setPeriodEnd(Date periodEnd) {
        this.periodEnd = periodEnd;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<TimeEntryView> getTimeEntries() {
        return timeEntries;
    }

    public void setTimeEntries(List<TimeEntryView> timeEntries) {
        this.timeEntries = timeEntries;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportForPeriodView that = (ReportForPeriodView) o;
        return Objects.equals(id, that.id) && Objects.equals(periodStart, that.periodStart) && Objects.equals(periodEnd, that.periodEnd) && status == that.status && Objects.equals(timeEntries, that.timeEntries);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, periodStart, periodEnd, status, timeEntries);
    }

    @Override
    public String toString() {
        return "ReportForPeriodView{" +
                "id=" + id +
                ", periodStart=" + periodStart +
                ", periodEnd=" + periodEnd +
                ", status=" + status +
                ", timeEntries=" + timeEntries +
                '}';
    }
}
