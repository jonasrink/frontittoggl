package dk.frontit.toggl.model;

public enum TaskType {
    VACATION, DAYOFF, ILLNESS
}
