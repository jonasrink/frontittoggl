package dk.frontit.toggl.service;

import dk.frontit.toggl.entity.ReportForPeriod;
import dk.frontit.toggl.entity.TimeEntry;
import dk.frontit.toggl.exception.NotFoundException;
import dk.frontit.toggl.model.ReportForPeriodView;
import dk.frontit.toggl.model.response.TimeEntryView;
import dk.frontit.toggl.repository.ReportForPeriodRepository;
import dk.frontit.toggl.service.toggl.TimeEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReportForPeriodService {

    @Autowired
    private TimeEntryService timeEntryService;

    @Autowired
    private ReportForPeriodRepository periodRepository;

    public ReportForPeriod savePeriod(ReportForPeriod reportForPeriod){
        return periodRepository.save(reportForPeriod);
    }

    public ReportForPeriodView mapReportForPeriodToView(ReportForPeriod reportForPeriod) {
        List<TimeEntryView> timeEntryViews = reportForPeriod.getTimeEntries().stream()
                .map(timeEntryService::mapTimeEntryToView)
                .collect(Collectors.toList());
        return new ReportForPeriodView(reportForPeriod.getId(), reportForPeriod.getPeriodStart(), reportForPeriod.getPeriodEnd(), reportForPeriod.getStatus(), timeEntryViews);
    }

    public ReportForPeriod getReportForId(long id) {
        return periodRepository.findById(id).orElseThrow(() -> new NotFoundException("Report with id:"+ id + " not found"));
    }

    public ReportForPeriod checkReportAvailabillity(Date since, Date until) {
        return periodRepository.findReportForPeriodByPeriodStartAndPeriodEnd(since, until);
    }


    private ReportForPeriod mapViewToEntity(ReportForPeriodView reportForPeriodView){
        List<TimeEntry> timeEntries = reportForPeriodView.getTimeEntries().stream()
                .map(timeEntryService::mapViewToEntity)
                .collect(Collectors.toList());
        return new ReportForPeriod(reportForPeriodView.getPeriodStart(), reportForPeriodView.getPeriodEnd(), reportForPeriodView.getStatus(), timeEntries);
    }
}
