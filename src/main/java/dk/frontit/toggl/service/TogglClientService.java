package dk.frontit.toggl.service;

import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class TogglClientService implements TogglClient {

    @Value("${toggl-api.token}")
    private String apiToken;
    @Value("${toggl-api.pass}")
    private String apiPass;

    @Autowired
    private OkHttpClientService okHttpClientService;

    public String sendGetRequest(String url) {
        final Request.Builder builder = new Request.Builder();
        final Request request = builder.url(url)
                .header("Authorization", Credentials.basic(apiToken, apiPass))
                .addHeader("Content-Type", "application/json")
                .build();
        return okHttpClientService.makeRequest(request);
    }



}
