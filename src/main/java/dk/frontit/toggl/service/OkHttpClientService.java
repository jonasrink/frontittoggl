package dk.frontit.toggl.service;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class OkHttpClientService {


    @Autowired
    private OkHttpClient okHttpClient;

    public String makeRequest(Request request) {
        try {
            Response response = okHttpClient.newCall(request).execute();
            ResponseBody body = response.body();
            String responseJson = body.string();
            body.close();
            response.close();
            return responseJson;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
