package dk.frontit.toggl.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.frontit.toggl.entity.Holidays;
import dk.frontit.toggl.model.PublicHolidaysType;
import dk.frontit.toggl.model.response.google.calendar.GoogleCalendarResponse;
import dk.frontit.toggl.model.response.google.calendar.Item;
import dk.frontit.toggl.repository.PublicHolidaysRepository;
import dk.frontit.toggl.service.toggl.ReportService;
import okhttp3.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CalendarService {
    Logger logger = LoggerFactory.getLogger(ReportService.class);
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    @Value("${google.api.calendar.url}")
    private String url;

    @Value("${google.api.calendar.token}")
    private String token;

    @Autowired
    private OkHttpClientService okHttpClientService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PublicHolidaysRepository holidaysRepository;

    public List<Holidays> findAll() {
        return holidaysRepository.findAll();
    }

    public List<Holidays> syncPublicHolidays() throws JsonProcessingException {
        final Request.Builder builder = new Request.Builder();
        final Request request = builder.url(new StringBuilder().append(url)
                .append("?key=").append(token).toString())
                .addHeader("Content-Type", "application/json")
                .build();
        try {
            GoogleCalendarResponse googleCalendarResponse = objectMapper.readValue(okHttpClientService.makeRequest(request), GoogleCalendarResponse.class);
            List<Holidays> holidays = googleCalendarResponse.getItems().stream()
                    .filter(item -> Arrays.stream(PublicHolidaysType.values()).anyMatch(publicHolidaysType -> publicHolidaysType.getSummary().equals(item.getSummary())))
                    .map(this::mapResponseItemToEnity)
                    .collect(Collectors.toList());
            holidays = holidays.stream()
                    .filter(holidays1 ->
                            findAll().stream()
                                    .noneMatch(holidays2 ->
                                            holidays1.getSummary().equals(holidays2.getSummary()) &&
                                                    holidays2.getSince().compareTo(holidays1.getSince()) == 0))
                    .collect(Collectors.toList());
            return holidaysRepository.saveAll(holidays);
        } catch (JsonProcessingException e) {
            logger.error("Exception acquired while parsing response to GoogleCalendarResponse", e);
            throw e;
        }
    }

    private Holidays mapResponseItemToEnity(Item item) {
        return new Holidays(item.getSummary(), LocalDate.parse(item.start.getDate()), LocalDate.parse(item.end.getDate()));
    }

}
