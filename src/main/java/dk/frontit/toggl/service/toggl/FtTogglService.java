package dk.frontit.toggl.service.toggl;

import com.fasterxml.jackson.core.JsonProcessingException;
import dk.frontit.toggl.entity.ReportForPeriod;
import dk.frontit.toggl.entity.TimeEntry;
import dk.frontit.toggl.entity.Users;
import dk.frontit.toggl.entity.UserTimeReport;
import dk.frontit.toggl.exception.UserWorkingTimeException;
import dk.frontit.toggl.model.*;
import dk.frontit.toggl.model.response.TimeEntryView;
import dk.frontit.toggl.model.response.UserView;
import dk.frontit.toggl.service.ReportForPeriodService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.codehaus.groovy.runtime.DefaultGroovyMethods.collect;

@Service
public class FtTogglService implements FtToggl {
    private static final Logger logger = LoggerFactory.getLogger(FtTogglService.class);
    private static final String SUBJECT_TEXT = "Toggl reminder for period from %s to %s";
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    @Value("${toggl-api.baseUrl}")
    private String baseUrlApi;

    @Value("${toggl.baseUrl.forEmail}")
    private String baseUrlForEmail;

    @Value("${toggl-api.user-agent}")
    private String userAgent;

    @Value("${toggl-api.workspace-id}")
    private String workSpaceId;

    @Autowired
    private ReportService reportService;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private TimeEntryService timeEntryService;

    @Autowired
    private ReportForPeriodService reportForPeriodService;

    @Autowired
    private UserTimeReportService timeReportService;

    @Override
//    @Transactional
    public ReportForPeriodView getTimeEntriesWithoutProjectForPeriod(Date since, Date until) throws Exception {
        try {
            List<Users> users = userService.syncUsers();
            ReportForPeriod reportForPeriod = null;
            List<TimeEntry> reportForWithoutProject = reportService.getReportForWithoutProject(since, until);
            ReportForPeriod reportForPeriodDb = aggregateReports(since, until, reportForWithoutProject);
            if (reportForPeriodDb != null) {
                reportForPeriod = reportForPeriodService.savePeriod(reportForPeriodDb);
            } else {
                List<TimeEntry> timeEntries = timeEntryService.saveAllTimeEntries(reportForWithoutProject);
                reportForPeriod = reportForPeriodService.savePeriod(new ReportForPeriod(since, until, Status.UNFIXED, null));
                ReportForPeriod finalReportForPeriod = reportForPeriod;
                timeEntries.forEach(timeEntry -> finalReportForPeriod.addTimeEntry(timeEntry));
                reportForPeriodService.savePeriod(finalReportForPeriod);
            }
            return reportForPeriodService.mapReportForPeriodToView(reportForPeriod);
        } catch (Exception e) {
            logger.error("Error while aggregating report", e);
            throw e;
        }
    }

    private ReportForPeriod aggregateReports(Date since, Date until, List<TimeEntry> reportForWithoutProject) {
        ReportForPeriod reportForPeriod = reportForPeriodService.checkReportAvailabillity(since, until);
        if (reportForPeriod != null) {

            reportForPeriod.getTimeEntries().stream()
                    .filter(timeEntry -> reportForWithoutProject.stream()
                            .noneMatch(timeEntry1 -> timeEntry.getId().equals(timeEntry1.getId())))
                    .collect(Collectors.toList())
                    .forEach(timeEntry -> timeEntry.setStatus(Status.FIXED));

            reportForPeriod.getTimeEntries().stream()
                    .filter(timeEntry -> reportForWithoutProject.stream()
                            .anyMatch(timeEntry1 -> timeEntry.getId().equals(timeEntry1.getId())))
                    .collect(Collectors.toList())
                    .forEach(timeEntry -> timeEntry.setStatus(Status.UNFIXED));

            reportForWithoutProject.removeIf(timeEntry -> reportForPeriod.getTimeEntries().stream()
                    .anyMatch(timeEntry1 -> timeEntry.getId() != timeEntry1.getId()));
            reportForPeriod.getTimeEntries().addAll(reportForWithoutProject);

//            reportForPeriod.getTimeEntries().stream()
//                    .filter(timeEntry -> finalReportForWithoutProject.stream()
//                            .anyMatch(timeEntry1 -> timeEntry.getId() != timeEntry1.getId()))
//                    .forEach(timeEntry -> timeEntry.setStatus(Status.FIXED));
////                    .filter(timeEntry -> finalReportForWithoutProject.stream()
////                            .anyMatch(timeEntry1 -> timeEntry.getId().equals(timeEntry1.getId()) &&
////                                    timeEntry1.getPid() != null))


            if (reportForPeriod.getTimeEntries().stream()
                    .allMatch(timeEntry -> timeEntry.getStatus().name().equals(Status.FIXED.name())) || reportForPeriod.getTimeEntries().size() == 0) {
                reportForPeriod.setStatus(Status.FIXED);
            } else {
                reportForPeriod.setStatus(Status.UNFIXED);
            }
//            timeEntryService.saveAllTimeEntries(reportForPeriod.getTimeEntries());
        }
        return reportForPeriod;
    }


    @Override
    public void sendEmailReminders(long id) {
        final ReportForPeriod reportForId = reportForPeriodService.getReportForId(id);
        Map<Users, List<TimeEntry>> userTimeEntryMap = reportForId.getTimeEntries().stream()
                .collect(Collectors.groupingBy(TimeEntry::getUser));
        userTimeEntryMap.entrySet().stream()
                .forEach(userListEntry -> sendEmailForUser(userListEntry, reportForId.getPeriodStart(), reportForId.getPeriodEnd()));
    }

    @Override
    public void sendEmailReminderForUser(ReportForPeriodView reportForPeriodView, List<UserView> userView) {

    }

    @Override
    public ReportForPeriodView gerReportForUser(ReportRequest reportRequest) throws JsonProcessingException, ParseException {
        String userIds = reportRequest.getUserIds().stream()
                .map(id -> String.valueOf(id))
                .collect(Collectors.joining(";"));
        List<TimeEntryView> reportForUserAndTimePeriod = reportService.getReportForUserAndTimePeriod(reportRequest);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        return new ReportForPeriodView(simpleDateFormat.parse(reportRequest.getSince()), simpleDateFormat.parse(reportRequest.getUntil()), null, reportForUserAndTimePeriod);
    }

    @Override
    public TimeReportView calculateSummary(ReportRequest reportRequest) throws Exception {
        try {
            List<Users> users = userService.syncUsers();
            if (users.size() > 0) {
                String usersToUpdate = users.stream()
                        .map(Users::getEmail)
                        .collect(Collectors.joining(";"));
                throw new UserWorkingTimeException("New users added after sync. Please set working time for users: " + usersToUpdate);
            }
            List<Users> allUsers = userService.getAllUsers();
            reportRequest.addUserId(allUsers.stream()
                    .map(Users::getId)
                    .collect(Collectors.toSet()));
            List<TimeEntryView> responseEntries = reportService.getReportForUserAndTimePeriod(reportRequest);
            Date since = simpleDateFormat.parse(reportRequest.getSince());
            Date until = simpleDateFormat.parse(reportRequest.getUntil());
            Map<Long, List<TimeEntryView>> usersTimeEntriesMap = responseEntries.stream()
                    .collect(Collectors.groupingBy(TimeEntryView::getUid));
            List<UserTimeReport> userTimeReports = usersTimeEntriesMap.entrySet().stream()
                    .map(longListEntry -> reportService.calculateUsersTimes(longListEntry.getKey(), longListEntry.getValue(), since, until ))
                    .collect(Collectors.toList());


            List<UserTimeReportView> userTimeReportViews = userTimeReports.stream()
                    .map(timeReportService::mapTimeReportEntityToView)
                    .collect(Collectors.toList());
            return new TimeReportView(since, until, null, null, userTimeReportViews);
        } catch (RuntimeException e) {
            logger.error(e.getMessage());
            throw e;
        }
    }

    private void sendEmailForUser(Map.Entry<Users, List<TimeEntry>> userListEntry, Date since, Date until) {
//        String collect = userListEntry.getValue().stream()
//                .map(timeEntryService::mapTimeEntryToView)
//                .map(TimeEntryView::toString)
//                .collect(Collectors.joining(";\n"));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String linkToToggl = String.format(baseUrlForEmail, workSpaceId, simpleDateFormat.format(since), simpleDateFormat.format(until), userListEntry.getKey().getId());
        emailService.sendMessage(userListEntry.getKey().getEmail(), String.format(SUBJECT_TEXT, simpleDateFormat.format(since), simpleDateFormat.format(until)), linkToToggl);
    }


}
