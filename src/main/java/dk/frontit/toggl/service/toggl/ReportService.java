package dk.frontit.toggl.service.toggl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.frontit.toggl.entity.*;
import dk.frontit.toggl.model.ReportRequest;
import dk.frontit.toggl.model.Status;
import dk.frontit.toggl.model.response.ReportResponse;
import dk.frontit.toggl.model.response.TimeEntryView;
import dk.frontit.toggl.service.CalendarService;
import dk.frontit.toggl.service.TogglClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;


@Service
public class ReportService {
    Logger logger = LoggerFactory.getLogger(ReportService.class);
    private static final Set<DayOfWeek> WEEKENDS = EnumSet.of(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);
    private static final long WORKING_DAYS_PER_WEEK = 5l;
    @Value("${toggl-api.baseUrl}")
    private String baseUrl;

    @Value("${toggl-api.user-agent}")
    private String userAgent;

    @Value("${toggl-api.workspace-id}")
    private String workSpaceId;
    @Value("${toggl-api.reportsUrl}")
    private String REPORTS_ENDPOINT;

    private String dateFormat = "yyyy-MM-dd";
    private final SimpleDateFormat dateFormater = new SimpleDateFormat(dateFormat);

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private TogglClientService togglClientService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private TimeEntryService timeEntryService;

    @Autowired
    private CalendarService calendarService;

    private static final List<Long> temporaryVacationList = new ArrayList<>();

    private static final List<Long> temporaryIllnessList = new ArrayList<>();

    static {
        temporaryVacationList.add(24917088l);
        temporaryVacationList.add(24917091l);
        temporaryIllnessList.add(24936108l);

        //testToggl
        temporaryVacationList.add(66994536l);
        temporaryVacationList.add(66993783l);
        temporaryIllnessList.add(66994535l);
    }


    public List<TimeEntryView> getReportForUserAndTimePeriod(ReportRequest reportRequest) throws JsonProcessingException {
        try {
            final String response = togglClientService.sendGetRequest(createURL() + reportRequest.toParamList());
            return objectMapper.readValue(response, ReportResponse.class).getTimeEntryViews();
        } catch (JsonProcessingException e) {
            logger.error("Exception acquired while parsing String to TimeEntryView", e);
            throw e;
        }

    }


    public List<TimeEntry> getReportForWithoutProject(Date since, Date until) throws JsonProcessingException {
        try {
            final String response = togglClientService.sendGetRequest(createURL() + new ReportRequest()
                    .setSince(dateFormater.format(since))
                    .setUntil(dateFormater.format(until))
                    .addProjectId(0l)
                    .toParamList());
            return objectMapper.readValue(response, ReportResponse.class).getTimeEntryViews().stream()
                    .map(this::mapViewToEntity)
                    .collect(Collectors.toList());
        } catch (JsonProcessingException e) {
            logger.error("Exception acquired while parsing String to TimeEntryView", e);
            throw e;
        }
    }

    public UserTimeReport calculateUsersTimes(Long userId, List<TimeEntryView> timeEntryViews, Date since, Date until) {
        Users users = userService.getUser(userId);
        UserTimeReport userTimeReport = new UserTimeReport();
        userTimeReport.setUser(users);
        List<TimeEntry> timeEntryList = timeEntryViews.stream().map(timeEntryService::mapViewToEntity)
                .collect(Collectors.toList());
        userTimeReport.setTimeEntries(timeEntryList);
        userTimeReport.setWorkedTime(timeEntryViews.stream()
                .filter(timeEntryView -> (timeEntryView.getTid() != null) ? temporaryIllnessList.stream().noneMatch(aLong -> timeEntryView.getTid().equals(aLong)) : true)
                .filter(timeEntryView -> (timeEntryView.getTid() != null) ? temporaryVacationList.stream().noneMatch(aLong -> timeEntryView.getTid().equals(aLong)) : true)
                .map(TimeEntryView::getDuration)
                .reduce(0l, Long::sum));
        userTimeReport.setBillableTime(timeEntryViews.stream()
                .filter(TimeEntryView::getBillable)
                .map(TimeEntryView::getDuration)
                .reduce(0l, Long::sum));
        userTimeReport.setVacation(timeEntryViews.stream()
                .filter(timeEntryView -> (timeEntryView.getTid() != null) ? temporaryVacationList.stream().anyMatch(aLong -> timeEntryView.getTid().equals(aLong)) : false)
                .map(TimeEntryView::getDuration)
                .reduce(0l, Long::sum));
        userTimeReport.setIllness(timeEntryViews.stream()
                .filter(timeEntryView -> (timeEntryView.getTid() != null) ? temporaryIllnessList.stream().anyMatch(aLong -> timeEntryView.getTid().equals(aLong)) : false)
                .map(TimeEntryView::getDuration)
                .reduce(0l, Long::sum));
        userTimeReport.setPlanedTime(calculatePlannedTime(since, until, users.getPlanedTime()));
        return userTimeReport;
    }


    private Long calculatePlannedTime(Date since, Date until, List<PlanedTime> hoursPerweek) {
        LocalDate sinceLocal = since.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate untilLocal = until.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        List<PlanedTime> planedTimes = hoursPerweek.stream()
                .filter(getWorkingTimeInPeriod(sinceLocal, untilLocal))
                .collect(Collectors.toList());
        return getWorkingHoursForPlanedTimePeriods(planedTimes, sinceLocal, untilLocal);
    }

    private long getWorkingHoursForPlanedTimePeriods(List<PlanedTime> planedTimes, LocalDate sinceLocal, LocalDate untilLocal) {
        return planedTimes.stream()
                .map(planedTime -> {
                    LocalDate until = planedTime.getUntil() == null ? untilLocal : planedTime.getUntil();
                    List<LocalDate> localDates = filterTimesWorkDaysAndHolidays(planedTime.getSince(), planedTime.getUntil(), sinceLocal, untilLocal);
                    LocalDate since = planedTime.getSince().compareTo(sinceLocal) > 0 ? planedTime.getSince() : sinceLocal;
                    return localDates.size() * (planedTime.getWorkHoursPerweek() / WORKING_DAYS_PER_WEEK) - reduceTimeByHourBeforeHolidays(since, until);
                })
                .reduce((aLong, aLong2) -> aLong + aLong2)
                .orElse(0l);
    }

    private List<LocalDate> filterTimesWorkDaysAndHolidays(LocalDate since, LocalDate until, LocalDate sinceLocal, LocalDate untilLocal) {
        if (until == null) {
            until = untilLocal;
        }
        return since.datesUntil(until)
                .filter(localDate -> sinceLocal.datesUntil(untilLocal).anyMatch(localDateForPeriod -> localDate.compareTo(localDateForPeriod) == 0))
                .filter(localDate -> !WEEKENDS.contains(localDate.getDayOfWeek()))
                .filter(localDate -> isPublicHoliday(localDate))
                .collect(Collectors.toList());
    }
    @CacheEvict(value = "publicHolidays", allEntries = true)
    public boolean isPublicHoliday(LocalDate localDate) {
        return calendarService.findAll().stream()
                .noneMatch(publicHolidays -> publicHolidays.getSince().compareTo(localDate) == 0 &&
                        publicHolidays.getUntil().compareTo(localDate) == 0);
    }

    private Predicate<PlanedTime> getWorkingTimeInPeriod(LocalDate since, LocalDate until) {
        return planedTime -> since.datesUntil(until.plusDays(1))
                .filter(localDate -> planedTime.getSince().compareTo(localDate) == 0 ||
                        (planedTime.getUntil() != null) ||
                        (planedTime.getUntil() != null && planedTime.getUntil().compareTo(localDate) == 0) ||
                        (planedTime.getSince().compareTo(localDate) <= 0 &&
                                (planedTime.getUntil() != null && planedTime.getUntil().compareTo(localDate) >= 0)) ||
                        (planedTime.getSince().compareTo(localDate) <= 0 &&
                                (planedTime.getUntil() == null)))
                .count() > 0;
    }


    private long reduceTimeByHourBeforeHolidays(LocalDate sinceLocal, LocalDate untilLocal) {
        return sinceLocal.datesUntil(untilLocal)
                .filter(publicHolidays ->
                        calendarService.findAll().stream()
                                .anyMatch(publicHolidays1 -> publicHolidays.plusDays(1l).compareTo(publicHolidays1.getSince()) == 0))
                .count() * 1000 * 60 * 60;
    }


    private Long getWorkingDays(LocalDate since, LocalDate until) {
        return since.datesUntil(until)
                .filter(d -> !WEEKENDS.contains(d.getDayOfWeek()))
                .filter(localDate -> isPublicHoliday(localDate))
                .count();

    }

    private String createURL() {
        return new StringBuilder(baseUrl).append(REPORTS_ENDPOINT)
                .append("workspace_id=").append(this.workSpaceId)
                .append("&user_agent=").append(this.userAgent)
                .toString();
    }

    private TimeEntry mapViewToEntity(TimeEntryView timeEntryView) {
        Users users = userService.getUser(timeEntryView.getUid());
        Project projectForId = null;
        if (timeEntryView.getPid() != null) {
            projectForId = projectService.getProjectForId(timeEntryView.getPid()).orElse(null);
        }
        return new TimeEntry(timeEntryView.getId(), timeEntryView.getDescription(), projectForId, timeEntryView.getStart(), timeEntryView.getStop(), timeEntryView.getDuration(), timeEntryView.getBillable(), timeEntryView.getClient(), timeEntryView.getWorkspace(), timeEntryView.getCreated_with(), timeEntryView.getDuronly(), timeEntryView.getPid(), timeEntryView.getWid(), timeEntryView.getTid(), Status.UNFIXED, users, timeEntryView.getTagNames());
    }
}
