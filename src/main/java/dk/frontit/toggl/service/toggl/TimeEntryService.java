package dk.frontit.toggl.service.toggl;

import dk.frontit.toggl.entity.Project;
import dk.frontit.toggl.entity.TimeEntry;
import dk.frontit.toggl.entity.Users;
import dk.frontit.toggl.model.response.TimeEntryView;
import dk.frontit.toggl.repository.TimeEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TimeEntryService {

    @Autowired
    private TimeEntryRepository timeEntryRepository;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserService userService;

    public TimeEntry saveTimeEntry(TimeEntry timeEntry) {
        return timeEntryRepository.save(timeEntry);
    }

    public List<TimeEntry> saveAllTimeEntries(List<TimeEntry> timeEntries) {
        List<TimeEntry> tempList = new ArrayList<>();
        timeEntries.forEach(timeEntry -> {
            timeEntryRepository.save(timeEntry);
            tempList.add(timeEntry);
            timeEntryRepository.flush();
        });
        return tempList;
    }

    public TimeEntryView mapTimeEntryToView(TimeEntry timeEntry) {
        String projectId = "";
        Long userId = null;
        if (timeEntry.getProject() != null) {

        }
        if (timeEntry.getUser() != null) {
            userId = timeEntry.getUser().getId();
        }
        return new TimeEntryView(timeEntry.getId(), timeEntry.getDescription(), projectId, timeEntry.getStart(), timeEntry.getStop(), timeEntry.getDuration(), timeEntry.getBillable(), timeEntry.getClient(), timeEntry.getWorkspace(), timeEntry.getTagNames(), timeEntry.getCreatedWith(), timeEntry.getDurOnly(), timeEntry.getPid(), timeEntry.getWid(), timeEntry.getTid(), userId, timeEntry.getStatus());
    }

    public TimeEntry mapViewToEntity(TimeEntryView timeEntryView) {
        Project project = null;
        Users users = null;
        if (timeEntryView.getUid() != null) {
            users = userService.getUser(timeEntryView.getUid());
        }
        return new TimeEntry(timeEntryView.getId(), timeEntryView.getDescription(), project, timeEntryView.getStart(), timeEntryView.getStop(), timeEntryView.getDuration(), timeEntryView.getBillable(), timeEntryView.getClient(), timeEntryView.getWorkspace(), timeEntryView.getCreated_with(), timeEntryView.getDuronly(), timeEntryView.getPid(), timeEntryView.getWid(), timeEntryView.getTid(), timeEntryView.getStatus(), users, timeEntryView.getTagNames());
    }
}
