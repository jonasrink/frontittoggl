package dk.frontit.toggl.service.toggl;

import dk.frontit.toggl.entity.UserTimeReport;
import dk.frontit.toggl.model.UserTimeReportView;
import dk.frontit.toggl.model.response.TimeEntryView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserTimeReportService {

    @Autowired
    private UserService userService;

    @Autowired
    private TimeEntryService timeEntryService;

    public UserTimeReportView mapTimeReportEntityToView(UserTimeReport userTimeReport){
        List<TimeEntryView> timeEntryViews = userTimeReport.getTimeEntries().stream()
                .map(timeEntryService::mapTimeEntryToView)
                .collect(Collectors.toList());
        return new UserTimeReportView(userTimeReport.getTimeReportId(), userService.mapUserEnitityToView(userTimeReport.getUser()),userTimeReport.getPlanedTime(), userTimeReport.getWorkedTime(), userTimeReport.getBillableTime(), userTimeReport.getDayOff(), userTimeReport.getVacation(), userTimeReport.getIllness(), timeEntryViews);
    }
}
