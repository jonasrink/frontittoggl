package dk.frontit.toggl.service.toggl;

import com.fasterxml.jackson.databind.ObjectMapper;
import dk.frontit.toggl.entity.Project;
import dk.frontit.toggl.model.response.ProjectView;
import dk.frontit.toggl.repository.ProjectRepository;
import dk.frontit.toggl.service.TogglClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectService {
    Logger logger = LoggerFactory.getLogger(UserService.class);
    @Value("${toggl-api.baseUrl}")
    private String baseUrl;

    @Value("${toggl-api.user-agent}")
    private String userAgent;

    @Value("${toggl-api.workspace-id}")
    private String workSpaceId;

    @Value("${toggl-api.projectUrl}")
    private String projectUrl;

    @Autowired
    private TogglClientService togglClientService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProjectRepository projectRepository;

    protected Optional<Project> getProjectForId(Long id) {
        return projectRepository.findById(id);
    }

    protected List<Project> syncProjects() {
//        try {
//            final List<Project> projectRepositoryAll = projectRepository.findAll();
//            final List<Project> projectList = getWorkspaceProjects().stream()
//                    .map(this::mapProjectViewToProject)
//                    .collect(Collectors.toList());
//            projectList.removeIf(project -> projectRepositoryAll.stream()
//                    .anyMatch(project1 -> project.getId() != project1.getId())
//            );
//            projectRepository.saveAll(projectList);
//            return projectList.size();
//        } catch (Exception e) {
//            logger.error("Exception acquired while syncing users DB with Toggl", e);
//            throw e;
//        }
        return null;
    }

    public List<ProjectView> getWorkspaceProjects() throws Exception {
        try {
            String s = togglClientService.sendGetRequest(baseUrl + String.format(projectUrl, workSpaceId));
            return Arrays.asList(objectMapper.readValue(s, ProjectView[].class));
        } catch (Exception e) {
            String msg = "Exception acquired while getting projects for workspace";
            logger.error(msg, e);
            throw new Exception(msg, e);
        }
    }
}
