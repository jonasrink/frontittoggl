package dk.frontit.toggl.service.toggl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dk.frontit.toggl.entity.Users;
import dk.frontit.toggl.exception.NotFoundException;
import dk.frontit.toggl.model.PlanedTimeView;
import dk.frontit.toggl.model.response.UserView;
import dk.frontit.toggl.repository.UserRepository;
import dk.frontit.toggl.service.TogglClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    Logger logger = LoggerFactory.getLogger(UserService.class);
    @Value("${toggl-api.baseUrl}")
    private String baseUrl;

    @Value("${toggl-api.user-agent}")
    private String userAgent;

    @Value("${toggl-api.workspace-id}")
    private String workSpaceId;

    @Value("${toggl-api.usersUrl}")
    private String userUrl;

    @Autowired
    private TogglClientService togglClientService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;


    public List<UserView> getWorkspaceUserList() throws RuntimeException {
        try {
            String s = togglClientService.sendGetRequest(baseUrl + String.format(userUrl, workSpaceId));
            return Arrays.asList(objectMapper.readValue(s, UserView[].class));
        } catch (RuntimeException e) {
            logger.error("Exception acquired while getting users for workspace", e);
            throw new RuntimeException("Exception acquired while getting users for workspace", e);
        } catch (JsonProcessingException e) {
            logger.error("Exception acquired while serializing object", e);
            throw new RuntimeException("Exception acquired while serializing object", e);
        }
    }

    public List<Users> syncUsers() throws RuntimeException {
        try {
            final List<Users> repositoryAll = userRepository.findAll();
            final List<Users> workspaceUsersList = getWorkspaceUserList().stream()
                    .map(this::mapUserViewToUser)
                    .collect(Collectors.toList());
            workspaceUsersList.removeIf(user -> repositoryAll.stream()
                    .anyMatch(user1 -> user.getId() != user1.getId())
            );
            return userRepository.saveAll(workspaceUsersList);

        } catch (RuntimeException e) {
            logger.error("Exception acquired while syncing users DB with Toggl", e);
            throw e;
        }
    }

    public Users mapUserViewToUser(UserView userView) {
        return new Users(userView.getId(), userView.getDefaultWid(), userView.getEmail(), userView.getFullname(), userView.getJqueryTimeofdayFormat(), userView.getJqueryDateFormat(), userView.getTimeofdayFormat(), userView.getDateFormat(), userView.isStoreStartAndStopTime(), userView.getBeginningOfWeek(), userView.getLanguage(), userView.getAt(), userView.getCreatedAt(), userView.getRetention(), userView.getTimezone());
    }

    public UserView mapUserEnitityToView(Users users) {
        List<PlanedTimeView> planedTimeView = null;
        if (users.getPlanedTime() != null) {
            planedTimeView = users.getPlanedTime().stream()
                    .map(planedTime -> new PlanedTimeView(planedTime.getId(), planedTime.getWorkHoursPerweek(), planedTime.getSince(), planedTime.getUntil(), null))
                    .collect(Collectors.toList());
        }
        return new UserView(users.getId(), users.getDefaultWid(), users.getEmail(), users.getFullname(), users.getJqueryTimeofdayFormat(), users.getJqueryDateFormat(), users.getTimeofdayFormat(), users.getDateFormat(), users.isStoreStartAndStopTime(), users.getBeginningOfWeek(), users.getLanguage(), users.getAt(), users.getCreatedAt(), users.getRetention(), users.getTimezone(), planedTimeView);
    }

    public Users getUser(Long uid) {
        return userRepository.findById(uid)
                .orElseThrow(() -> new NotFoundException("User with id:" + uid + " not found"));
    }

    public List<Users> getAllUsers() {
        return userRepository.findAll();
    }


    public List<Users> saveAll(List<Users> allUsers) {
        return userRepository.saveAll(allUsers);
    }
}
