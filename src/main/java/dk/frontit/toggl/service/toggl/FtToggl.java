package dk.frontit.toggl.service.toggl;

import com.fasterxml.jackson.core.JsonProcessingException;
import dk.frontit.toggl.entity.ReportForPeriod;
import dk.frontit.toggl.model.ReportForPeriodView;
import dk.frontit.toggl.model.ReportRequest;
import dk.frontit.toggl.model.TimeReportView;
import dk.frontit.toggl.model.response.UserView;
import org.springframework.http.ResponseEntity;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface FtToggl {

    public ReportForPeriodView getTimeEntriesWithoutProjectForPeriod(Date since, Date until) throws Exception;

    public void sendEmailReminders(long id);
    public void sendEmailReminderForUser(ReportForPeriodView reportForPeriodView, List<UserView> userView);

    ReportForPeriodView gerReportForUser(ReportRequest reportRequest) throws JsonProcessingException, ParseException;

    TimeReportView calculateSummary(ReportRequest reportRequest) throws Exception;
}
