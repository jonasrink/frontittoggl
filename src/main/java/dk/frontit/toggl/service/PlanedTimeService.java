package dk.frontit.toggl.service;

import dk.frontit.toggl.entity.PlanedTime;
import dk.frontit.toggl.entity.Users;
import dk.frontit.toggl.exception.NotFoundException;
import dk.frontit.toggl.model.PlanedTimeView;
import dk.frontit.toggl.model.response.UserView;
import dk.frontit.toggl.repository.PlanedTimeRepository;
import dk.frontit.toggl.service.toggl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlanedTimeService {

    @Autowired
    private PlanedTimeRepository planedTimeRepository;

    @Autowired
    private UserService userService;

    public PlanedTime createPlanedTime(PlanedTimeView planedTimeView, long id) {
        PlanedTime planedTime = mapViewToEntity(planedTimeView);
        planedTime.setUser(userService.getUser(id));
        return planedTimeRepository.save(planedTime);
    }

    public PlanedTimeView mapEntityToView(PlanedTime planedTime) {
        UserView userView = userService.mapUserEnitityToView(planedTime.getUser());
        return new PlanedTimeView(planedTime.getId(), planedTime.getWorkHoursPerweek(), planedTime.getSince(), planedTime.getUntil(), userView);
    }

    public PlanedTime mapViewToEntity(PlanedTimeView planedTimeView) {
        Users users = null;
        if (planedTimeView.getUserView() != null) {
            users = userService.mapUserViewToUser(planedTimeView.getUserView());
        }
        return new PlanedTime(planedTimeView.getWorkHoursPerweek(), planedTimeView.getSince(), planedTimeView.getUntil(), users);
    }

    public List<PlanedTime> findAll() {
        return planedTimeRepository.findAll();
    }

    public PlanedTime findById(long id) {
        return planedTimeRepository.findById(id).orElseThrow(() -> new NotFoundException("PlanedTime with id: "+ id + " not found"));
    }

    public void deleteById(long id) {
        planedTimeRepository.deleteById(id);
    }

    public PlanedTime updatePlanedTime(PlanedTimeView planedTimeView, long id) {
        PlanedTime planedTime = findById(id);
        planedTime.setUntil(planedTimeView.getUntil());
        planedTime.setWorkHoursPerweek(planedTimeView.getWorkHoursPerweek());
        planedTime.setSince(planedTimeView.getSince());
        return planedTimeRepository.save(planedTime);
    }
}
