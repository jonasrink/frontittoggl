package dk.frontit.toggl.repository;


import dk.frontit.toggl.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Long> {
}
