package dk.frontit.toggl.repository;

import dk.frontit.toggl.entity.Holidays;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PublicHolidaysRepository extends JpaRepository<Holidays, Long> {
}
