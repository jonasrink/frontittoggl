package dk.frontit.toggl.repository;

import dk.frontit.toggl.entity.ReportForPeriod;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;

public interface ReportForPeriodRepository extends JpaRepository<ReportForPeriod, Long> {

    public ReportForPeriod findReportForPeriodByPeriodStartAndPeriodEnd(Date periodStart, Date periodEnd);
}
