package dk.frontit.toggl.repository;

import dk.frontit.toggl.entity.PlanedTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface PlanedTimeRepository extends JpaRepository<PlanedTime, Long> {

    @Override
    @Transactional
    @Modifying
    @Query("delete from PlanedTime pt where pt.id = :id")
    void deleteById(@Param("id") Long id);
}
