package dk.frontit.toggl.interceptor;

import okhttp3.*;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

public class RestClientLoggingInterceptor implements Interceptor {
    Logger logger = LoggerFactory.getLogger(RestClientLoggingInterceptor.class);

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException { //todo make it to log out request/response
        Request request = chain.request();

        long t1 = System.nanoTime();
        logger.info(String.format("Sending request %s on %s",
                request.url(), request.headers()));

        Response response = chain.proceed(request);
        MediaType contentType = response.body().contentType();
        if(contentType.type().equals("text") && contentType.subtype().equals("html")){
            return response;
        }
        InputStreamReader inputStreamReader = new InputStreamReader(response.body().byteStream(), StandardCharsets.UTF_8);

        String collect = new BufferedReader(inputStreamReader).lines().collect(Collectors.joining());

        String responseString = new StringBuilder()
                .append("Response: ( code:")
                .append(response.code())
                .append(" ")
                .append(collect)
                .append(")")
                .toString();
        long t2 = System.nanoTime();
        logger.info(String.format("Received response for %s in %.1fms%n%s",
                response.request().url(), (t2 - t1) / 1e6d, responseString));
        ResponseBody wrappedBody = ResponseBody.create(contentType, collect);
        return response.newBuilder().body(wrappedBody).build();
    }
}
