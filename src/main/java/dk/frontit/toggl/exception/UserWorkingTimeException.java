package dk.frontit.toggl.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class UserWorkingTimeException extends RuntimeException {
    public UserWorkingTimeException(String s) {
        super(s);
    }
}
