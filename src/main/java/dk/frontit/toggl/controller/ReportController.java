package dk.frontit.toggl.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import dk.frontit.toggl.entity.TimeReport;
import dk.frontit.toggl.model.ReportForPeriodView;
import dk.frontit.toggl.model.ReportRequest;
import dk.frontit.toggl.model.TimeReportView;
import dk.frontit.toggl.model.response.TimeEntryView;
import dk.frontit.toggl.service.toggl.FtTogglService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@Api
@Controller
@RequestMapping("/api/report")
public class ReportController {

    @Autowired
    private FtTogglService ftTogglService;

    @PostMapping(path = "/get-report-for-without-project")
    public ResponseEntity<ReportForPeriodView> getTimeEntriesWithoutProject(@RequestBody ReportRequest reportForPeriodView) throws Exception {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return ResponseEntity.ok(ftTogglService.getTimeEntriesWithoutProjectForPeriod(simpleDateFormat.parse(reportForPeriodView.getSince()), simpleDateFormat.parse(reportForPeriodView.getUntil())));
    }

    @GetMapping(path = "/send-reminder/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void getTimeEntriesWithoutProject(@PathVariable(name = "id") long reportId) {
        ftTogglService.sendEmailReminders(reportId);
    }

    @PostMapping(path = "/user")
    public ResponseEntity<ReportForPeriodView> getReportForUser(@RequestBody ReportRequest reportRequest) throws ParseException, JsonProcessingException {
        return ResponseEntity.ok(ftTogglService.gerReportForUser(reportRequest));
    }

    @PostMapping(path = "/summary")
    public ResponseEntity<TimeReportView> compareHoursPlannedWorkded(@RequestBody ReportRequest reportRequest) throws Exception {
        TimeReportView timeReportView = ftTogglService.calculateSummary(reportRequest);
        return ResponseEntity.ok(timeReportView);
    }
}
