package dk.frontit.toggl.controller;

import dk.frontit.toggl.model.response.UserView;
import dk.frontit.toggl.service.toggl.UserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.stream.Collectors;

@Api
@Controller
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(path = "/sync-with-toggl")
    public ResponseEntity<List<UserView>> syncWithToggl(){
        return ResponseEntity.ok(userService.syncUsers().stream()
                .map(userService::mapUserEnitityToView)
                .collect(Collectors.toList()));
    }

    @GetMapping(path = "/get-all")
    public ResponseEntity<List<UserView>> getAllUsers(){
        return ResponseEntity.ok(userService.getAllUsers().stream()
                .map(userService::mapUserEnitityToView)
                .collect(Collectors.toList()));
    }
}
