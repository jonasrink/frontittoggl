package dk.frontit.toggl.controller;

import dk.frontit.toggl.entity.PlanedTime;
import dk.frontit.toggl.model.PlanedTimeView;
import dk.frontit.toggl.service.PlanedTimeService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Api
@RestController
@RequestMapping("/api/planedtime")
public class PlanedTimeController {

    @Autowired
    private PlanedTimeService planedTimeService;

    @PostMapping(path = "/create-planedtime/{id}")
    public ResponseEntity<PlanedTimeView> createPlanedTime(@RequestBody PlanedTimeView planedTimeView, @PathVariable("id") long id){
        PlanedTime planedTime = planedTimeService.createPlanedTime(planedTimeView, id);
        return ResponseEntity.ok(planedTimeService.mapEntityToView(planedTime));
    }

    @GetMapping(path = "/")
    public ResponseEntity<List<PlanedTimeView>> findAllPlanedTimes(){
        List<PlanedTime> all = planedTimeService.findAll();
        return ResponseEntity.ok(all.stream()
                .map(planedTimeService::mapEntityToView)
                .collect(Collectors.toList()));
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<PlanedTimeView> findById(@PathVariable("id") long id){
        PlanedTimeView planedTimeView = planedTimeService.mapEntityToView(planedTimeService.findById(id));
        return ResponseEntity.ok(planedTimeView);
    }

    @DeleteMapping(path = "/delete-planedtime/{id}")
    public void deleteById(@PathVariable("id") long id){
        planedTimeService.deleteById(id);
    }

    @PostMapping(path = "/update-planedtime/{id}")
    public ResponseEntity<PlanedTimeView> updatePlanedTime(@RequestBody PlanedTimeView planedTimeView, @PathVariable("id") long id){
        PlanedTimeView updatedPlanedTime = planedTimeService.mapEntityToView(planedTimeService.updatePlanedTime(planedTimeView, id));
        return ResponseEntity.ok(updatedPlanedTime);
    }
}
