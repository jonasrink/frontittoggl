package dk.frontit.toggl.config;

import dk.frontit.toggl.interceptor.RestClientLoggingInterceptor;
import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class OkHttpClient3Configuration {

    @Bean
    public OkHttpClient okHttpClient(@Value("${okhttpClient.timeout}") Long timeout, @Value("${okhttpClient.maxRequestPerHost}") int maxRequestPerHost) {
        return new OkHttpClient().newBuilder()
                .addInterceptor(new RestClientLoggingInterceptor())
                .callTimeout(timeout, TimeUnit.SECONDS)
                .dispatcher(createDispatcher(maxRequestPerHost))
                .build();
    }

    Dispatcher createDispatcher(int maxRequestPerHost) {
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setMaxRequestsPerHost(maxRequestPerHost);
        return dispatcher;
    }


}
