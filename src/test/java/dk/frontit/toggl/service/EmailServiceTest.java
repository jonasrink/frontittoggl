package dk.frontit.toggl.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import dk.frontit.toggl.model.response.TimeEntryView;
import dk.frontit.toggl.service.toggl.EmailService;
import dk.frontit.toggl.service.toggl.ReportService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Calendar;
import java.util.List;


@SpringBootTest
public class EmailServiceTest {

    @Autowired
    EmailService emailService;

    @Autowired
    ReportService reportService;

    @Test
    public void shouldSendEmail() throws JsonProcessingException {
        Calendar cal = Calendar.getInstance();
        Calendar calUntil = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 3);
        cal.set(Calendar.DAY_OF_MONTH, 21);
        calUntil.set(Calendar.MONTH, 3);
        calUntil.set(Calendar.DAY_OF_MONTH, 21);
//        List<TimeEntryView> reportForUserAndTimePeriod = reportService.getReportForUserAndTimePeriodWithoutProject("5679972", cal.getTime(), calUntil.getTime());
//        reportForUserAndTimePeriod.stream()
//                .map(timeEntry -> emailService.sendMessage(timeEntry.get))
        emailService.sendMessage("jonas.rinkevicius@gmail.com", "toggl-test", "no text");
    }

}