package dk.frontit.toggl.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import dk.frontit.toggl.entity.Holidays;
import dk.frontit.toggl.entity.PlanedTime;
import dk.frontit.toggl.entity.Users;
import dk.frontit.toggl.model.ReportForPeriodView;
import dk.frontit.toggl.model.ReportRequest;
import dk.frontit.toggl.model.TimeReportView;
import dk.frontit.toggl.service.toggl.FtTogglService;
import dk.frontit.toggl.service.toggl.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;


@SpringBootTest
public class FtTogglServiceTest {

    @Autowired
    private FtTogglService ftTogglService;

    @Autowired
    private UserService userService;

    @Autowired
    private CalendarService calendarService;

    @Test
    public void shouldSyncUserAndFindTimeEntriesWOProjectsAndSendEMailToUsers() throws Exception {
        Calendar cal = Calendar.getInstance();
        Calendar calUntil = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 3);
        cal.set(Calendar.DAY_OF_MONTH, 21);
        calUntil.set(Calendar.MONTH, 3);
        calUntil.set(Calendar.DAY_OF_MONTH, 28);
        ReportForPeriodView timeEntriesWithoutProjectForPeriod = ftTogglService.getTimeEntriesWithoutProjectForPeriod(cal.getTime(), calUntil.getTime());
        ftTogglService.sendEmailReminders(timeEntriesWithoutProjectForPeriod.getId());
    }

    @Test
    public void shouldCalculateWorkingHoursPerUserForPeriod() throws Exception {
        ReportRequest reportRequest = new ReportRequest()
                .setSince("2021-04-01")
                .setUntil("2021-04-30");
        TimeReportView timeEntriesWithoutProjectForPeriod = ftTogglService.calculateSummary(reportRequest);
        timeEntriesWithoutProjectForPeriod.getUsersTimeReportViews()

                .forEach(userTimeReportView -> {
                    System.out.println("billable: " + getTime(userTimeReportView.getBillableTime()));
                    System.out.println("workedTime: " + getTime(userTimeReportView.getWorkedTime()));
                    System.out.println("illness: " + getTime(userTimeReportView.getIllness()));
                    System.out.println("vacation: " + getTime(userTimeReportView.getVacation()));
                    System.out.println("plannedTime: " + getTime(userTimeReportView.getPlanedTime()));
                    System.out.println(userTimeReportView.toString());
                });

    }

    private String getTime(Long millis) {
        return String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }

    @BeforeEach
    void setUp() throws JsonProcessingException {
        List<Holidays> holidays = calendarService.syncPublicHolidays();
        userService.syncUsers();
        List<Users> allUsers = userService.getAllUsers();
        allUsers.forEach(user -> {
            user.addPlanedTime(new PlanedTime(72000000l,LocalDate.parse("2021-03-01"), user));
            user.addPlanedTime(new PlanedTime(144000000l,LocalDate.parse("2021-04-15"), user));
        });
        userService.saveAll(allUsers);
    }
}