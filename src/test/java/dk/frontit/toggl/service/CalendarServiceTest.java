package dk.frontit.toggl.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import dk.frontit.toggl.entity.Holidays;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.List;

@SpringBootTest
public class CalendarServiceTest {

    @Autowired
    private CalendarService calendarService;

    @Test
    public void shouldReturnAListOfPublicHolidays() throws JsonProcessingException {
        List<Holidays> holidays = calendarService.syncPublicHolidays();
        holidays.forEach(holidays1 -> System.out.println(holidays1));
        Assert.notEmpty(holidays);

    }
}
