package dk.frontit.toggl.service;


import dk.frontit.toggl.service.toggl.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;


@SpringBootTest
public class UsersServiceTest {

    @Autowired
    UserService userService;

    @Test
    public void shouldReturnAllUsersOfWorkspace() throws Exception {
        userService.getWorkspaceUserList().forEach(System.out::println);
    }

    @Test
    public void shouldGetUsersFromTogglAndSaveToDBReturnNumberOfSavedUsers() throws Exception {
        int i = userService.syncUsers().size();
        Assert.isTrue(i > 0);
        i = userService.syncUsers().size();
        Assert.isTrue(i == 0);
    }

}