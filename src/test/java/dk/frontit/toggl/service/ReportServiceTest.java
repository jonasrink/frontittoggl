package dk.frontit.toggl.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import dk.frontit.toggl.entity.TimeEntry;
import dk.frontit.toggl.model.ReportRequest;
import dk.frontit.toggl.model.response.TimeEntryView;
import dk.frontit.toggl.service.toggl.ReportService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.Calendar;
import java.util.List;


@SpringBootTest
public class ReportServiceTest {

    @Autowired
    ReportService reportService;

    @Test
    public void shouldReturnTimeEntriesForUser() throws JsonProcessingException {
        Calendar cal = Calendar.getInstance();
        Calendar calUntil = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 3);
        cal.set(Calendar.DAY_OF_MONTH, 27);
        calUntil.set(Calendar.MONTH, 3);
        calUntil.set(Calendar.DAY_OF_MONTH, 28);
        List<TimeEntryView> reportForUserAndTimePeriod = reportService.getReportForUserAndTimePeriod(new ReportRequest().setSince(cal.getTime().toString()).setUntil(calUntil.getTime().toString()));
//        List<TimeEntryView> reportForUserAndTimePeriod = reportService.getReportForUserAndTimePeriod("333435", cal.getTime(), calUntil.getTime());
        reportForUserAndTimePeriod.forEach(System.out::println);
    }
    @Test
    public void shouldReturnTimeEntriesForUserWithoutProject() throws JsonProcessingException {
        Calendar cal = Calendar.getInstance();
        Calendar calUntil = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2021);
        cal.set(Calendar.MONTH, 3);
        cal.set(Calendar.DAY_OF_MONTH, 21);
        calUntil.set(Calendar.MONTH, 3);
        calUntil.set(Calendar.DAY_OF_MONTH, 23);
        List<TimeEntry> reportForUserAndTimePeriod = reportService.getReportForWithoutProject( cal.getTime(), calUntil.getTime());
        Assert.notEmpty(reportForUserAndTimePeriod);
    }

}